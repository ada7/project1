float bruteForce(point P[], int n,float m1,point m[]) 
{ 
	float min = FLT_MAX;
	 
	for (int i = 0; i < n; ++i)
	{ 
		for (int j = i+1; j < n; ++j) 
		{
			min=dist(P[i],P[j]);
			if(min<m1)
			{
			    m[0]=P[i];
			    m[1]=P[j];
			    m1=min;
			}
	    }
	}
			
	return m1; 
}
