void closestpair(int n,struct point p[])
{
    float distance;
    int dminimum,i,j,x1,x2,y1,y2;
    dminimum=9999;
    for(i=0;i<n;i++)
    {
        for(j=i+1;j<n;j++)
        {
            distance=dist(p[i],p[j]);
            if(distance<dminimum)
            {
                dminimum=distance;
                x1=p[i].x;
                x2=p[j].x;
                y1=p[i].y;
                y2=p[j].y;
            }
        }
    }
    printf("\nThe shortest distance: %f",dminimum);
    printf("\nThe closest pair among the given pairs is (%d,%d) and (%d,%d)\n",x1,y1,x2,y2);
}