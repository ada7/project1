#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<float.h> 

#include "structure_divide.h"
#include "distance_formula_divide_conquer.h"
#include "brute_divide_conquer.h"
#include "find_closed_pair.h"


 
int main()
{
    int i,n;
    point p[100],min[50];
    float m=FLT_MAX;
   
    
    printf("\nEnter the number of points:");
    scanf("%d",&n);
    
     printf("\nEnter the points\n");
    for(i=0;i<n;i++)
    {
        scanf("%f%f",&p[i].x,&p[i].y);
    }
    
    int low=0,high=n-1;
    min[0].x=0.0;
    min[0].y=0.0;
    min[1].x=0.0;
    min[1].y=0.0;
    
    m=find(p,low,high,min,n,m);
   
    printf("\nclosest pair is (%f,%f),(%f,%f)\n",min[0].x,min[0].y,min[1].x,min[1].y);
    printf("\nMinimum distance is %f\n",m);
   
    return 0;
}
    
